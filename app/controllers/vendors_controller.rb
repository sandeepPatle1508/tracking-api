class VendorsController < ApplicationController
  before_filter :filter_model_attributes, :only => [:create, :update]

  # GET /vendors
  # GET /vendors.json
  def index
    @vendors = Vendor.all

    render json: @vendors
  end

  # GET /vendors/1
  # GET /vendors/1.json
  def show
    @vendor = Vendor.find(params[:id])

    render json: @vendor
  end

  # POST /vendors
  # POST /vendors.json
  def create
    @vendor = Vendor.new(@model_attributes)

    if @vendor.save
      render :json => @vendor
    else
      render json: @vendor.errors, status: :unprocessable_entity
    end    
  end

  # PUT /vendors/1
  # PUT /vendors/1.json
  def update
    @vendor = Vendor.find(params[:id])

    if @vendor.update_attributes(@model_attributes)
      render :json => {:message => 'Vendor was successfully updated.'}
    else
      render json: @vendor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vendors/1
  # DELETE /vendors/1.json
  def destroy
    @vendor = Vendor.find(params[:id])
    @vendor.destroy

    render :json => {:message => "Vendor was successfully deleted."}
  end
end
