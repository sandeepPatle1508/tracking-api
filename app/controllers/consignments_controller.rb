class ConsignmentsController < ApplicationController
  before_filter :filter_model_attributes, :only => [:create, :update]

  # GET /consignments
  # GET /consignments.json
  def index
    @consignments = Consignment.all
    render json: @consignments
  end

  # GET /consignments/1
  # GET /consignments/1.json
  def show
    @consignment = Consignment.find(params[:id])
    render json: @consignment
  end

  # POST /consignments
  # POST /consignments.json
  def create
    @consignment = Consignment.new(@model_attributes)

    if @consignment.save        
      render json: @consignment, status: :created, location: @consignment
    else        
      render json: @consignment.errors, status: :unprocessable_entity
    end
  end

  # PUT /consignments/1
  # PUT /consignments/1.json
  def update
    @consignment = Consignment.find(params[:id])

    if @consignment.update_attributes(@model_attributes)        
      render :json => {:message => 'Consignment was successfully updated.'}
    else        
      render json: @consignment.errors
    end    
  end

  # DELETE /consignments/1
  # DELETE /consignments/1.json
  def destroy
    @consignment = Consignment.find(params[:id])
    @consignment.destroy
    render :json => {:message => 'Consignment was successfully deleted.'}
  end
end
