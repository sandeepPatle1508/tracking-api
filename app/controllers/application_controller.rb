class ApplicationController < ActionController::Base
  protect_from_forgery

  def filter_model_attributes
    @model_attributes = params.dup
    @model_attributes.delete('controller')
    @model_attributes.delete('action')
    @model_attributes.delete('id')
    @model_attributes.delete('updated_at')
    @model_attributes.delete('created_at')
  end
end
