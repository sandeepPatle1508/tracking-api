class ConsignmentHistoriesController < ApplicationController
  before_filter :filter_model_attributes, :only => [:create, :update]

  # GET /vendors
  # GET /vendors.json
  def index
    if params[:consignment_id].present?
      @consignment_histories = ConsignmentHistory.where(consignment_id: params[:consignment_id])
    else
      @consignment_histories = ConsignmentHistory.all
    end

    render json: @consignment_histories
  end

  # GET /vendors/1
  # GET /vendors/1.json
  def show
    @consignment_history = ConsignmentHistory.find(params[:id])

    render json: @consignment_history
  end

  # POST /consignment_histories
  # POST /consignment_histories.json
  def create
    @consignment_history = ConsignmentHistory.new(@model_attributes)

    if @consignment_history.save
      render json: @consignment_history, status: :created, location: @consignment_history
    else
      render json: @consignment_history.errors, status: :unprocessable_entity
    end
  end

  # PUT /consignment_histories/1
  # PUT /consignment_histories/1.json
  def update
    @consignment_history = ConsignmentHistory.find(params[:id])

    if @consignment_history.update_attributes(@model_attributes)
      render :json => {:message => 'Your comment was successfully updated.'}
    else  
      render json: @consignment_history.errors, status: :unprocessable_entity
    end
    
  end

  # DELETE /consignment_histories/1
  # DELETE /consignment_histories/1.json
  def destroy
    @consignment_history = ConsignmentHistory.find(params[:id])
    @consignment_history.destroy

    render :json => {:message => 'Your comment was successfully deleted.'}
  end
end
