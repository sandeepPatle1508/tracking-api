class OrganizationsController < ApplicationController
  before_filter :filter_model_attributes, :only => [:create, :update]

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.all
    render json: @organizations
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    @organization = Organization.find(params[:id])
    render json: @organization
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(@model_attributes)

    if @organization.save
      render :json => @organization
    else
      render json: @organization.errors, status: :unprocessable_entity
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.json
  def update
    @organization = Organization.find(params[:id])

    if @organization.update_attributes(@model_attributes)
      render :json => {:message => 'Organization was successfully updated.'}
    else
      render json: @organization.errors, status: :unprocessable_entity
    end    
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization = Organization.find(params[:id])
    @organization.destroy

    render :json => {:message => "Organization was successfully deleted."}
  end
end
