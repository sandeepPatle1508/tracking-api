class Consignment < ActiveRecord::Base
  attr_accessible :category_id, :organization_id, :title, :type_id, :vendor_id, :sent_date, :qr_code, :received_date
end
