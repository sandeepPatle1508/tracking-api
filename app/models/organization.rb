class Organization < ActiveRecord::Base
  attr_accessible :address, :city, :country, :name, :phone_number, :zip_code
end
