require 'test_helper'

class ConsignmentHistoriesControllerTest < ActionController::TestCase
  setup do
    @consignment_history = consignment_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consignment_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consignment_history" do
    assert_difference('ConsignmentHistory.count') do
      post :create, consignment_history: { comment: @consignment_history.comment, commenter_id: @consignment_history.commenter_id, consignment_id: @consignment_history.consignment_id }
    end

    assert_redirected_to consignment_history_path(assigns(:consignment_history))
  end

  test "should show consignment_history" do
    get :show, id: @consignment_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consignment_history
    assert_response :success
  end

  test "should update consignment_history" do
    put :update, id: @consignment_history, consignment_history: { comment: @consignment_history.comment, commenter_id: @consignment_history.commenter_id, consignment_id: @consignment_history.consignment_id }
    assert_redirected_to consignment_history_path(assigns(:consignment_history))
  end

  test "should destroy consignment_history" do
    assert_difference('ConsignmentHistory.count', -1) do
      delete :destroy, id: @consignment_history
    end

    assert_redirected_to consignment_histories_path
  end
end
