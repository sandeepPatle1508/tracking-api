# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150314150545) do

  create_table "consignment_histories", :force => true do |t|
    t.integer  "consignment_id"
    t.integer  "commenter_id"
    t.text     "comment"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "consignments", :force => true do |t|
    t.integer  "vendor_id"
    t.integer  "organization_id"
    t.string   "title"
    t.integer  "type_id"
    t.integer  "category_id"
    t.datetime "sent_date"
    t.datetime "received_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.text     "qr_code"
  end

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "city"
    t.string   "country"
    t.string   "phone_number"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "zip_code"
  end

  create_table "vendors", :force => true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "city"
    t.string   "country"
    t.string   "zip_code"
    t.string   "phone_number"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

end
