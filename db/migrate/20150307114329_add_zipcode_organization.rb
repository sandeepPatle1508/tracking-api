class AddZipcodeOrganization < ActiveRecord::Migration
  def up
  	add_column :organizations, :zip_code, :string
  end

  def down
  	remove_column :organizations, :zip_code
  end
end
