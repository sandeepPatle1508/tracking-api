class AddQrCodeToConsignment < ActiveRecord::Migration
  def up
  	add_column :consignments, :qr_code, :text
  end

  def down
  	remove_column :consignments, :qr_code
  end
end
