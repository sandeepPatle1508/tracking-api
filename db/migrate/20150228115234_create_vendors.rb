class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :name
      t.text :address
      t.string :city
      t.string :country
      t.string :zip_code
      t.string :phone_number

      t.timestamps
    end
  end
end
