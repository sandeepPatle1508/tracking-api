class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.text :address
      t.string :city
      t.string :country
      t.string :phone_number

      t.timestamps
    end
  end
end
