class CreateConsignments < ActiveRecord::Migration
  def change
    create_table :consignments do |t|
      t.integer :vendor_id
      t.integer :organization_id
      t.string :title
      t.integer :type_id
      t.integer :category_id
      t.datetime :sent_date
      t.datetime :received_date

      t.timestamps
    end
  end
end
