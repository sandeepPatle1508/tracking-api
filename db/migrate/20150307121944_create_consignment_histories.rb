class CreateConsignmentHistories < ActiveRecord::Migration
  def change
    create_table :consignment_histories do |t|
      t.integer :consignment_id
      t.integer :commenter_id
      t.text :comment

      t.timestamps
    end
  end
end
